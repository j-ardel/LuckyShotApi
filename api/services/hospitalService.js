const https = require('https');
const ClosestHospitalBaseUrl = "https://ws-spsl-ext-non-auth.webapp.health.nsw.gov.au/rted/api/GetNearestHospitalsByAddress?";
const HospitalDetailsUrl = "https://ws-spsl-ext-non-auth.webapp.health.nsw.gov.au/rted/api/GetHospitalDetails/";
const MaxHospitalsPerRequest = 10;
const Hospital = require('./../models/hospitalModel');

exports.findClosestHospital = (location, callback) => {

  let url = ClosestHospitalBaseUrl +
    "latitude=" + location.lat +
    "&longitude=" + location.lng;

  let data = "";
  https.get(url, (response) => {
    response.on('data', (_chunks) => {
      data += _chunks
    });

    response.on('end', () => {

      let hospital = JSON.parse(data)[0];
      https.get(HospitalDetailsUrl + hospital.hospitalID, (response) => {
        let hospitalData = "";
        response.on('data', (_chunks) => {
          hospitalData += _chunks
        });

        response.on('end', () => {
          const hospitalDetails = JSON.parse(hospitalData);
          let closestHospital = new Hospital(hospitalDetails);
          console.log(closestHospital);
          hospitalDetails.reportingHospitalDetails
          .slice(0, MaxHospitalsPerRequest)
          .map(hospital => {

            console.log(hospital);
          });
        });
      });
      callback(JSON.parse(data)[0]);
    })
  })
};