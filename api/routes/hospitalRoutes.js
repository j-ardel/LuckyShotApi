'use strict';
module.exports = function (app) {
  var hospitalController = require('../controllers/hospitalController');

  app.route('/hospitals').get(hospitalController.listHospitals);
  app.route('/hospital/closest').get(hospitalController.calculateTime);
};
